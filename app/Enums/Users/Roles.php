<?php


namespace App\Enums\Users;

use App\Enums\Enum;

abstract class Roles extends Enum
{
    const OWNER = "Owner";
    const DEVELOPER = "Developer";
    const MANAGER = "Manager";
    const ADMIN = "Admin";
}
